package com.example.proj;

import java.io.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "auth-servlet", value = "/auth")
public class AuthServlet extends HttpServlet 
{
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException 
	{
        String n = request.getParameter("username");
        String p = request.getParameter("userpass");

        ConnectionStatus cs1 = AuthDAO.validateAdmin(n, p);
        ConnectionStatus cs2 = AuthDAO.validateUser(n, p);

        if (cs1 == ConnectionStatus.WRONG_PASSWORD || cs2 == ConnectionStatus.WRONG_PASSWORD)
		{
            RequestDispatcher rd = request.getRequestDispatcher("wrong-login-servlet");
            rd.forward(request, response);
        } 
		else if (cs1 == ConnectionStatus.OK) 
		{
            RequestDispatcher rd = request.getRequestDispatcher("admin-servlet");
            rd.forward(request, response);
        } 
		else if (cs2 == ConnectionStatus.OK) 
		{
            RequestDispatcher rd = request.getRequestDispatcher("welcome-servlet");
            rd.forward(request, response);
        } 
		else 
		{
            RequestDispatcher rd = request.getRequestDispatcher("wrong_login.jsp");
            rd.include(request, response);
        }
    }

    public void destroy() 
	{
		
    }
}